# window.py
#
# Copyright 2019 Paulo Queiroz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio

import subprocess
import threading
import time
import sys

from usage.preferences import UsagePreferences
import usage.password as Password
import usage.constants as Constants

# Get real max CPU freq
# sudo dmidecode -t processor | grep Speed

class _Module:
    """ 'Interface' for monitor modules. """
    def __init__(self, window):
        self.window = window

    def build_ui(self):
        pass

    def update(self):
        pass


class PowerInfo(_Module):

    perf_radio = None
    auto_radio = None
    save_radio = None

    modifying = False
    has_boost = False
    boosted = False

    ct = 5 # This is used to update only on % 5 (every 5 seconds)

    def build_ui(self):
        self.perf_radio = self.window.perf_radio
        self.auto_radio = self.window.auto_radio
        self.save_radio = self.window.save_radio

        self.perf_radio.connect('toggled', self.on_change)
        self.auto_radio.connect('toggled', self.on_change)
        self.save_radio.connect('toggled', self.on_change)

        self.has_boost = self._get_has_boost()
        self.window.boost_box.set_property('sensitive', self.has_boost)

        if self.has_boost:
            self.boosted = self._get_boost()
            self.window.boost_switch.set_active(self.boosted)
            self.window.boost_switch.connect('notify::active',
                                             self._on_boost_switch_change)

        pass

    def update(self):
        if self.modifying:
            return

        if self.ct % 5 != 0:
            self.ct += 1
            return

        self.ct = 1

        p = subprocess.Popen(
            'cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor',
            stdout=subprocess.PIPE,
            shell=True
        )
        out = ((p.communicate())[0]).decode('utf-8').strip()

        self.modifying = True

        if out == 'ondemand':
            self.auto_radio.set_active(True)
        elif out == 'powersave':
            self.save_radio.set_active(True)
        elif out == 'performance':
            self.perf_radio.set_active(True)
        else:
            print('Unsupported power save option %s' % out)
            sys.exit(1)

        self._mode = out
        self.modifying = False

        pass

    def _get_has_boost(self, *args):
        p = subprocess.Popen('test -f /sys/devices/system/cpu/cpufreq/boost',
                             shell=True)
        res = p.wait()
        return res == 0

    def _get_boost(self, *args):
        p = subprocess.Popen('cat /sys/devices/system/cpu/cpufreq/boost',
                             shell=True,
                             stdout=subprocess.PIPE)
        out = ((p.communicate())[0]).decode('utf-8').strip()
        return out == "1"

    def _on_boost_switch_change(self, *args):
        btn = self.window.boost_switch
        swi = btn.get_active()

        if swi != self.boosted:
            pw  = Password.get_password()
            pwd = 'pkexec ' if pw is None else 'echo {} | sudo -S '.format(pw)
            cmd = 'sh -c "echo {} | tee /sys/devices/system/cpu/cpufreq/boost"'\
                    .format('1' if swi else '0')

            p = subprocess.Popen(pwd + cmd,
                                 shell=True,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT)
            p.communicate()
            ret = p.wait()
            if ret != 0:
                print('Failed to set boost, exited with code {}'.format(ret))
                sys.exit(ret)

        self.boosted = swi
        pass

    def on_change(self, *args):
        if self.modifying:
            return

        self.modifying = True
        mode = None

        if self.perf_radio.get_active():
            mode = 'performance'
        elif self.auto_radio.get_active():
            mode = 'ondemand'
        else:
            mode = 'powersave'

        if mode != self._mode:
            self._mode = mode

            _pass = 'pkexec '
            cmd = 'sh -c "echo {} | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor"'.format(mode)

            pw = Password.get_password()
            if pw is not None:
                _pass = 'echo {} | sudo -S -- '.format(pw)

            p = subprocess.Popen(
                _pass + cmd,
                stdout=subprocess.PIPE,
                shell=True
            )
            p.communicate()

            ret = p.wait()

            if ret != 0:
                print('Invalid exit code for power save (%d)' % ret)
                sys.exit(1)

        self.modifying = False

        pass


class CpuInfo(_Module):

    cpu_labels = []

    def build_ui(self):
        out = self._get_cpu_freq()

        for i in range(0, len(out)):
            b = Gtk.Box(Gtk.Orientation.HORIZONTAL, 6)
            lbl = Gtk.Label(label=None)
            lbl.set_markup('<b>CPU{}:</b>'.format(i))
            lbl.set_xalign(1)
            lbl.set_size_request(120, -1)

            b.pack_start(lbl, False, True, 0)

            val = (float(out[i]) / 1000)
            txt = '%.2f GHz' % val

            lbl = Gtk.Label(label=txt)
            lbl.set_xalign(0)
            b.pack_start(lbl, True, True, 0)

            self.cpu_labels.append(lbl)

            b.show_all()

            self.window.cpu_info_box.pack_start(b, False, True, 0)

        self.window.cpu_modelname_label.set_label(self._get_cpu_name())

        pass

    def update(self):
        out = self._get_cpu_freq()
        avg = 0.0

        for i in range(0, len(out)):

            val = (float(out[i]) / 1000)
            avg = avg + val
            txt = '%.2f GHz' % val
            self.cpu_labels[i].set_label(txt)

        avg = avg / len(out)
        self.window.cpu_avg_freq_label.set_label('%.2f GHz' % avg)

        pass

    def _get_cpu_freq(self):
        p = subprocess.Popen(
            "grep -E '^cpu MHz' /proc/cpuinfo | grep -oE '([0-9]{1,6}.[0-9]{1,6})'",
            stdout=subprocess.PIPE,
            shell=True
        )
        out = ((p.communicate())[0]).decode('utf-8').strip().split()

        return (out)

    def _get_cpu_name(self):
        p = subprocess.Popen(
            "grep 'model name' /proc/cpuinfo | head -n 1 | cut -d ':' -f 2",
            stdout=subprocess.PIPE,
            shell=True
        )
        out = ((p.communicate())[0]).decode('utf-8').strip()

        return (out)

class GpuInfo(_Module):

    gpu_labels = []

    def build_ui(self):
        self.window.gpu_3d_renderer_label.set_label(self._get_gpu_3d_renderer())

        total, free = self._get_dedicated_mem()
        self.window.gpu_memory_label.set_label("{} | {} free".format(total, free))

        self.window.gpu_gl_version_label.set_label(self._get_gl_version())

        pass

    def update(self):
        total, free = self._get_dedicated_mem()
        self.window.gpu_memory_label.set_label("{} | {} free".format(total, free))
        pass

    def _get_gpu_3d_renderer(self):
        p = subprocess.Popen(
            "glxinfo | grep 'OpenGL renderer' | cut -d ':' -f 2",
            stdout=subprocess.PIPE,
            shell=True
        )
        out = ((p.communicate())[0]).decode('utf-8').strip()

        return (out)

    def _get_dedicated_mem(self):
        p = subprocess.Popen(
            "glxinfo | grep 'Dedicated video memory:' | cut -d ':' -f 2",
            stdout=subprocess.PIPE,
            shell=True
        )
        total = ((p.communicate())[0]).decode('utf-8').strip()

        p = subprocess.Popen(
            "glxinfo | grep 'Currently available dedicated video memory:' | cut -d ':' -f 2",
            stdout=subprocess.PIPE,
            shell=True
        )
        free = ((p.communicate())[0]).decode('utf-8').strip()

        return (total, free)

    def _get_gl_version(self):
        p = subprocess.Popen(
            "glxinfo | grep 'OpenGL core profile version string:' | cut -d ':' -f 2",
            stdout=subprocess.PIPE,
            shell=True
        )
        out = ((p.communicate())[0]).decode('utf-8').strip()

        return out


@Gtk.Template(resource_path='/com/raggesilver/Usage/layouts/window.ui')
class UsageWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'UsageWindow'

    cpu_info_box        = Gtk.Template.Child()
    cpu_modelname_label = Gtk.Template.Child()
    cpu_temp_label      = Gtk.Template.Child()
    cpu_avg_freq_label  = Gtk.Template.Child()

    gpu_3d_renderer_label   = Gtk.Template.Child()
    gpu_memory_label        = Gtk.Template.Child()
    gpu_gl_version_label    = Gtk.Template.Child()

    header_bar      = Gtk.Template.Child()
    side_header_bar = Gtk.Template.Child()
    side_box        = Gtk.Template.Child()

    perf_radio = Gtk.Template.Child()
    auto_radio = Gtk.Template.Child()
    save_radio = Gtk.Template.Child()

    boost_box       = Gtk.Template.Child()
    boost_switch    = Gtk.Template.Child()

    _modules = []

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._build_ui()
        self._add_modules()
        self._start_modules()

        action = Gio.SimpleAction.new('about', None)
        action.connect('activate', self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new('preferences', None)
        action.connect('activate', self.on_preferences)
        self.add_action(action)

        pass

    def _build_ui(self):
        provider = Gtk.CssProvider()
        provider.load_from_resource(
            '/com/raggesilver/Usage/resources/style.css');

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        settings = Gtk.Settings.get_default()
        settings.connect('notify::gtk-decoration-layout',
                         self.on_decoration_layout)
        self.on_decoration_layout()

        self.side_box.connect('size-allocate', self.on_side_box_allocate)
        self.side_header_bar.connect('size-allocate',
                                     self.on_side_header_bar_allocate)

        pass

    def on_about(self, *args):
        builder = Gtk.Builder.new_from_resource(
            '/com/raggesilver/Usage/layouts/about.ui'
        )

        dialog = builder.get_object('about_dialog')
        dialog.set_transient_for(self)
        dialog.set_attached_to(self)
        dialog.set_version(Constants.VERSION)
        dialog.run()
        dialog.destroy()

        pass

    _pref_window = None
    def on_preferences(self, *args):
        if self._pref_window is not None:
            return

        win = UsagePreferences()
        win.set_transient_for(self)
        win.set_attached_to(self)
        win.connect('destroy', self._on_pref_close)
        self._pref_window = win
        win.show()

        pass

    def _on_pref_close(self, *args):
        self._pref_window = None

    def on_side_box_allocate(self, *args):
        w = self.side_box.get_allocated_width()
        ww = self.side_header_bar.get_allocated_width()
        if w < ww:
            self.side_box.set_size_request(ww, -1)

        pass

    def on_side_header_bar_allocate(self, *args):
        w = self.side_header_bar.get_allocated_width()
        ww = self.side_box.get_allocated_width()
        if w < ww:
            self.side_header_bar.set_size_request(ww, -1)
        elif w > ww:
            self.side_box.set_size_request(w, -1)

        pass

    def on_decoration_layout(self, *args):
        settings = Gtk.Settings.get_default()
        decoration = settings.get_property('gtk-decoration-layout')

        if decoration.find(':menu') != -1:
            # Close on left
            self.header_bar.set_show_close_button(False)
            self.side_header_bar.set_show_close_button(True)
            self.header_bar.set_title("Usage")
            self.side_header_bar.set_title("")
        else:
            self.side_header_bar.set_show_close_button(False)
            self.header_bar.set_show_close_button(True)
            self.side_header_bar.set_title("Usage")
            self.header_bar.set_title("")

        self.on_side_header_bar_allocate()
        pass

    def _add_modules(self):
        self._modules.append(CpuInfo(self))
        self._modules.append(GpuInfo(self))
        self._modules.append(PowerInfo(self))

        for mod in self._modules:
            mod.build_ui()

        pass

    def _start_modules(self):
        self._t = threading.Thread(target=self._loop)
        self._t.daemon = True
        self._t.start()

        pass

    def _loop(self):
        while True:
            for mod in self._modules:
                mod.update()
            time.sleep(1)
        pass
