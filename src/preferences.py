# preferences.py
#
# Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GObject

import usage.password as Password

@Gtk.Template(resource_path='/com/raggesilver/Usage/layouts/preferences.ui')
class UsagePreferences(Gtk.ApplicationWindow):
    __gtype_name__ = 'UsagePreferences'

    password_button = Gtk.Template.Child()
    _sudo_window    = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        pw = Password.get_password()

        if pw is not None:
            Password.validate(pw, self._on_password_validate_cb)

        pass

    @Gtk.Template.Callback('on_password_button_clicked')
    def _on_pw_btn_click(self, *args):
        if self._sudo_window is not None:
            return

        self._sudo_window = UsageSudo()
        self._sudo_window.set_transient_for(self)
        self._sudo_window.set_attached_to(self)
        self._sudo_window.show()

        self._sudo_window.connect('destroy', self._on_sudo_window_destroy)
        self._sudo_window.connect('on-save', self._on_password_set)
        pass

    PW_SCHEMA = None
    def _set_password_schema(self, *args):
        self.PW_SCHEMA = Secret.Schema.new("com.raggesilver.Usage",
            Secret.SchemaFlags.NONE,
            {
                "number": Secret.SchemaAttributeType.INTEGER,
                "string": Secret.SchemaAttributeType.STRING,
                "even": Secret.SchemaAttributeType.BOOLEAN
            }
        )
        pass

    def _on_sudo_window_destroy(self, *args):
        self._sudo_window = None
        pass

    def _on_password_set(self, _widget, password):
        if password is not None:
            res = Password.set_password(password)
            if res == False:
                print('Could not set password')
        else:
            res = Password.clear_password()
            if res == False:
                print('Could not clear password')
        pass

    def _on_password_validate_cb(self, res):
        if not res:
            Password.clear_password()
        pass

@Gtk.Template(resource_path='/com/raggesilver/Usage/layouts/sudo.ui')
class UsageSudo(Gtk.ApplicationWindow):
    __gtype_name__ = 'UsageSudo'

    __gsignals__   = {
        'on-cancel': (GObject.SIGNAL_RUN_FIRST, None, ()),
        'on-save': (GObject.SIGNAL_RUN_FIRST, None, (str,))
    }

    password_entry = Gtk.Template.Child()
    save_button    = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        pass

    _pw = None
    @Gtk.Template.Callback('on_save')
    def _on_save(self, *args):
        text = self.password_entry.get_text()

        self.password_entry.get_style_context().remove_class('error')

        if text != '':
            self._pw = text
            Password.validate(text, self._validate_cb)
        else:
            self.emit('on-save', None)
            self.destroy()
        pass

    def _validate_cb(self, res):
        if res:
            self.emit('on-save', self._pw)
            self._pw = None
            self.destroy()
        else:
            print('Invalid password')
            self.password_entry.get_style_context().add_class('error')
            self.password_entry.set_text('')

    @Gtk.Template.Callback('on_cancel')
    def _on_cancel(self, *args):
        self.emit('on-cancel')
        self.destroy()
        pass

