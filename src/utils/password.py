# password.py
#
# Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import gi
import subprocess
import threading

gi.require_version('Secret', '1')

from gi.repository import Secret

SCHEMA = Secret.Schema.new("com.raggesilver.Usage",
    Secret.SchemaFlags.NONE,
    {
        "number": Secret.SchemaAttributeType.INTEGER,
        "string": Secret.SchemaAttributeType.STRING,
        "even": Secret.SchemaAttributeType.BOOLEAN
    }
)

_attr = {
    "number": "42",
    "string": "lapotato",
    "even": "false"
}

def set_password(pw):
    # TODO: try/except
    Secret.password_store_sync(SCHEMA, _attr, Secret.COLLECTION_DEFAULT,
                               "The label", pw, None)

def get_password():
    return Secret.password_lookup_sync(SCHEMA, _attr, None)

def clear_password():
    return Secret.password_clear_sync(SCHEMA, _attr, None)

def validate(pw, cb):
    if not pw:
        return False

    def _run_in_thread(cb):
        p = subprocess.Popen(
            'echo {} | sudo -kS -- sleep 0'.format(pw),
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT
        )

        ret = p.wait()
        cb(ret == 0)

    t = threading.Thread(target=_run_in_thread, args=(cb,))
    t.start()

    return t
    
