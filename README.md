<div align="center">
    <h1>
        <img src="https://gitlab.com/raggesilver/usage/raw/master/data/icons/hicolor/scalable/apps/com.raggesilver.Usage.svg" /> Usage
    </h1>
    <h4>A tiny usage monitor</h4>
    <p>
        <a href="https://www.patreon.com/raggesilver">
            <img src="https://img.shields.io/badge/patreon-donate-orange.svg?logo=patreon" alt="Proton on Patreon" />
        </a>
    </p>
    <p>
        <a href="#install">Install</a> •
        <a href="#features">Features</a> •
        <a href="https://gitlab.com/raggesilver/usage/blob/master/COPYING">License</a>
    </p>
</div>

Tiny side project that gives useful info not provided by GNOME Monitor on the
CPU, GPU and power mode.

**Disclaimer about the name**: There is already a GNOME app called Usage, and
I have no intention on claiming the name. `com.raggesilver.Usage` is a temporary
name I'm using until I come up with something better (open to suggestions).

![Preview](assets/preview.png)

# Install

Usage is not flatpak'ed 😥️, so old install it is.

```bash
git clone https://gitlab.com/raggesilver/usage
cd usage
meson _build
ninja -C _build
sudo ninja -C install
```

If you ever want to remove it just
```bash
sudo ninja -C uninstall
```

**Dependencies**

- meson (>= 0.50.0) | `sudo dnf install meson` on Fedora
- glx-utils (>= 8.4.0) | `sudo dnf install glx-utils` on Fedora (installed by default)
- libsecret | `sudo dnf install libsecret` on Fedora (installed by default?)

# Features

- CPU name
- CPU frequency monitor (+ average)
- CPU Boost (more info at
[kernel docs](https://www.kernel.org/doc/Documentation/cpu-freq/boost.txt))
- GPU memory/free memory
- GPU 3d renderer
- OpenGL version
- Power settings
- Save password*

*A few commands ran by Usage require `sudo` privileges. Those commands usually
propmt asking for the password (using `pkexec`), but `v0.1.1` introduces a way
to store your password using `libsecret` so that you don't need to retype your
password over and over again.

<div align="center">
    <img src="assets/sudo-preview.png" alt="sudo preview">
</div>

# Todo

- RAM info
- RAM usage
